package com.hadwinling.article.service;

import com.hadwinling.article.entity.Article;

import java.util.List;

/**
 * @author HadwinLing
 * @version V1.0
 * @Package com.hadwinling.article.service
 * @date 2020/10/10 11:01
 */
public interface ArticleService {
    List<Article> selectAllArticle();

    Integer selectViewcountById(Integer id);

    int updateViewcountById(Integer updatedViewcount, Integer id);

    int insertSelective(Article article);

    int deleteById(Integer id);


}


