package com.hadwinling.article.service.impl;

import com.hadwinling.article.entity.Article;
import com.hadwinling.article.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.hadwinling.article.mapper.ArticleMapper;
import com.hadwinling.article.service.ArticleService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author HadwinLing
 * @version V1.0
 * @Package com.hadwinling.article.service.impl
 * @date 2020/10/10 11:01
 */
@Service
@Transactional
public class ArticleServiceImpl implements ArticleService {

    @Resource
    private ArticleMapper articleMapper;
    @Autowired
    private RedisUtil redisUtil;

    @Override
    public List<Article> selectAllArticle() {
        List<Article> allArticle = articleMapper.selectAllArticle();
        return allArticle;
    }

    @Override
    public Integer selectViewcountById(Integer id) {
        boolean haskey = redisUtil.hasKey("viewcount");
        if (haskey){
            int viewcount = (Integer) redisUtil.get("viewcount");
            return viewcount;

        }
        int viewcount = articleMapper.selectViewcountById(id);
        redisUtil.set("viewcount",viewcount);
        return viewcount;
    }

    @Override
    public int deleteById(Integer id) {
        return articleMapper.deleteById(id);
    }

    @Override
    @CachePut(cacheNames = "article")
    public int insertSelective(Article article) {
        return articleMapper.insertSelective(article);
    }

    @Override
    public int updateViewcountById(Integer updatedViewcount, Integer id) {
        return articleMapper.updateViewcountById(updatedViewcount, id);
    }

}


