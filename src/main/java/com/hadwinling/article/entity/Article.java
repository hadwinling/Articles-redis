package com.hadwinling.article.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author HadwinLing
 * @version V1.0
 * @Package com.hadwinling.article.entity
 * @date 2020/10/10 20:22
 */
@Data
public class Article implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 标题
     */
    private String title;

    /**
     * 文章内容
     */
    private String article;

    /**
     * 浏览量
     */
    private Integer viewcount;
}