package com.hadwinling.article.controller;

import com.hadwinling.article.entity.Article;
import com.hadwinling.article.service.ArticleService;
import com.hadwinling.article.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author HadwinLing
 * @version V1.0
 * @Package com.hadwinling.article.controller
 * @date 2020/10/8 17:15
 */
@Controller
@ResponseBody
public class ArticleController {
    @Autowired
    private ArticleService articleService;
    @Autowired
    private RedisUtil redisUtil;

    @GetMapping("/findall")
    public List<Article>  selectAllArticles(){
        List<Article> allArticle = articleService.selectAllArticle();
        return allArticle;
    }
    @GetMapping("/find/{id}")
    public int findViewCountById(@PathVariable(name = "id") int id){
        int articleViewCount= articleService.selectViewcountById(id);
        return articleViewCount;
    }

    @GetMapping("/update/{id}")
    public int updateArticleById(@PathVariable(name = "id") int id ){
        return articleService.updateViewcountById(1,id);
    }

    @GetMapping("/add")
    public int addArticle(){
        Article article = new Article();
        article.setTitle("ss");
        article.setViewcount(0);
        article.setArticle("好文章");
        return  articleService.insertSelective(article);
    }

    @GetMapping("/delete/{id}")
    public int deleteArticle(@PathVariable(name = "id") int id){
        return articleService.deleteById(id);
    }

}
