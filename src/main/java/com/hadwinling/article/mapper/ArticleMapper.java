package com.hadwinling.article.mapper;

import com.hadwinling.article.entity.Article;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;import java.util.List;

/**
 * @author HadwinLing
 * @version V1.0
 * @Package com.hadwinling.article.mapper
 * @date 2020/10/10 20:22
 */
@Mapper
public interface ArticleMapper {
    Integer selectViewcountById(@Param("id")Integer id);

    List<Article> selectAllArticle();

    int insertSelective(Article article);

    int updateViewcountById(@Param("updatedViewcount")Integer updatedViewcount,@Param("id")Integer id);


    int deleteById(@Param("id")Integer id);


}